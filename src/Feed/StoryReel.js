import React, {Component} from 'react';
import Story from "./Story";
import "./StoryReel.css";

class StoryReel extends Component {
    render() {
        return (
            <div className={"storyReel"}>
                <Story img={"https://images.unsplash.com/photo-1559310589-2673bfe16970?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"}
                       profileSrc={"https://image.freepik.com/free-vector/cute-boy-standing-position-showing-thumb_96037-450.jpg"}
                       title={"Jonny Rikuls"}
                />
                <Story img={"https://media.gettyimages.com/photos/city-street-amidst-buildings-picture-id1006891820?s=612x612"}
                       profileSrc={"https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1080&fit=max&ixid=eyJhcHBfaWQiOjEyMDd9"}
                       title={"Mario Geovanni"}
                />
                <Story img={"https://wallpaperaccess.com/full/2393239.jpg"}
                       profileSrc={"https://ricogruppen.no/wp-content/uploads/2014/01/portrait-o.jpg"}
                       title={"Marco Polo"}
                />
                <Story img={"https://image.freepik.com/free-photo/city-sunset_1127-2123.jpg"}
                       profileSrc={"https://images.iphonephotographyschool.com/24755/portrait-photography.jpg"}
                       title={"Frida Jensen"}
                />
                <Story img={"https://norwaytravelguide.imgix.net/193026/x/0/7-things-to-do-amp-see-in-oslo-the-capital-city-of-norway-1?auto=compress%2Cformat&ch=Width%2CDPR&dpr=1&ixlib=php-3.3.0&w=883&s=4a4153a2868db23fa3ad490d28685088"}
                       profileSrc={"https://img.freepik.com/free-photo/portrait-white-man-isolated_53876-40306.jpg?size=626&ext=jpg"}
                       title={"Ola Normann"}
                />
            </div>
        );
    }
}

export default StoryReel;