import React, {Component} from 'react';
import "./Story.css";
import {Avatar} from "@material-ui/core";

class Story extends Component {
    render() {
        const img = this.props.img;
        const profileSrc = this.props.profileSrc;
        const title = this.props.title;
        const backgroundStyle = { backgroundImage: 'url('+ img + ')'}
        return (

            <div style={backgroundStyle} className="story">
                <Avatar className={"story__avatar"} src={profileSrc} />
                <h4>{title}</h4>
            </div>
        );
    }
}

export default Story;