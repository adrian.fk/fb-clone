import React, {useState} from 'react';
import "./Poster.css"
import {Avatar} from "@material-ui/core";
import VideocamIcon from '@material-ui/icons/Videocam';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import {useStateValue} from "../StateProvider";
import db from "../firebase";
import firebase from "firebase";



function Poster() {
    const [input, setInput] = useState("");
    const [imgUrl, setImgUrl] = useState("");
    const [{user}, dispatch] = useStateValue();

    const onSubmit = (e) => {
        e.preventDefault();

        const likers = [];

        db.collection("posts").add({
            message: input,
            timestamp: firebase.firestore.FieldValue.serverTimestamp(),
            profilePic: user.photoURL,
            username: user.displayName,
            image: imgUrl,
            likers: likers,
        })

        setInput("");
        setImgUrl("");
    }


    return (
        <div className={"messages"}>
            <div className={"messages__top"}>
                <Avatar src={user.photoURL}/>
                <form onSubmit={onSubmit}>
                    <input value={input}
                           onChange={(e) => setInput(e.target.value)}
                           className={"messages__input"}
                           placeholder={"What's on your mind, " + user.displayName + "?.. "}
                    />
                    <input
                        value={imgUrl}
                        onChange={(e) => setImgUrl(e.target.value)}
                        placeholder={"image URL (Optional)"}/>
                    <button type={"submit"}>Submit</button>
                </form>
            </div>
            <div className={"messages__bottom"}>
                <div className={"messages__option"}>
                    <VideocamIcon style={{ color: "red" }} />
                    <h3>Live Video</h3>
                </div>
                <div className={"messages__option"}>
                    <PhotoLibraryIcon style={{ color: "green" }} />
                    <h3>Photo/Video</h3>
                </div>
                <div className={"messages__option"}>
                    <InsertEmoticonIcon style={{ color: "orange" }} />
                    <h3>Feeling/Activity</h3>
                </div>
            </div>
        </div>
    );
}

export default Poster;