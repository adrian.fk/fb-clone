import React from 'react';
import "./Post.css"
import {Avatar} from "@material-ui/core";
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ChatBubbleOutlineIcon from '@material-ui/icons/ChatBubbleOutline';
import NearMeIcon from '@material-ui/icons/NearMe';
import {ExpandMoreOutlined} from '@material-ui/icons';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import db from "../firebase";
import {useStateValue} from "../StateProvider";

const GRAY = "gray";
const BLUE = "cornflowerblue";


function Post({ keyID, profilePic, image, username, timestamp, message, likers}) {
    const [{ user }, dispatch] = useStateValue();
    let postLiked = likers.includes(user.displayName);
    let postLikedColor = GRAY;


    function removeLiker(displayName) {
        likers = likers.filter(liker => liker !== displayName);
        handleLikeColor();
    }

    function isLiked() {
        postLiked = likers.includes(user.displayName);
        return postLiked;
    }

    const handleLikeColor = () => {
        if (isLiked()) postLikedColor = BLUE;
        else postLikedColor = GRAY;
    }

    const onLikeClick = (e) => {
        e.preventDefault();

        if (postLiked) {
            removeLiker(user.displayName);
        }
        else {
            likers.push(user.displayName);
        }

        if (!image) image = "";

        db.collection("posts").doc(keyID).set({
            username: username,
            profilePic: profilePic,
            image: image,
            message: message,
            timestamp: timestamp,
            likers: likers,
        })
            .then()
            .catch();

        handleLikeColor();
    }
    handleLikeColor();

    return (
        <div className={"post"}>
            <div className={"post__top"}>
                <Avatar src={profilePic}
                        className={"post__avatar"} />
                <div className={"post__topInfo"}>
                    <h3>{username}</h3>
                    <p>{new Date(timestamp?.toDate()).toUTCString()}</p>
                </div>
            </div>
            <div className={"post__bottom"}>
                <p>{message}</p>
            </div>
            <div className={"post__image"}>
                <img src={image} alt={""} />
            </div>
            <div className={"post__options"}>
                   <div className={"post__option"} onClick={onLikeClick}>
                       <ThumbUpIcon style={{fill: postLikedColor}}/>
                       <p style={{color: postLikedColor}}>Like</p>
                   </div>
                <div className={"post__option"}>
                    <ChatBubbleOutlineIcon />
                    <p>Comment</p>
                </div>
                <div className={"post__option"}>
                    <NearMeIcon />
                    <p>Share</p>
                </div>
                <div className={"post__option"}>
                    <AccountCircleIcon />
                    <ExpandMoreOutlined />
                </div>
            </div>
        </div>
    );
}

export default Post;