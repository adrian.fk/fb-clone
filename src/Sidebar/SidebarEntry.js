import React, {Component} from 'react';
import "./SidebarEntry.css";
import {Avatar} from "@material-ui/core";

class SidebarEntry extends Component {
    render() {
        const Icon = this.props.Icon;
        return (
            <div className="SidebarEntry">
                {this.props.src && <Avatar src={this.props.src} /> }
                {Icon && <Icon />}

                <h4>{this.props.title}</h4>
            </div>
        );
    }
}

export default SidebarEntry;