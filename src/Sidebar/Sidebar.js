import React, {Component} from 'react';
import "./Sidebar.css";
import SidebarEntry from "./SidebarEntry";
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import EmojiFlagsIcon from '@material-ui/icons/EmojiFlags';
import PeopleIcon from '@material-ui/icons/People';
import ChatIcon from '@material-ui/icons/Chat';
import StoreIcon from '@material-ui/icons/Store';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {useStateValue} from "../StateProvider";

function Sidebar() {
    const [{ user }, dispatch] = useStateValue();

    return (
        <div className="sidebar">
            <SidebarEntry src={user.photoURL} title={user.displayName} />
            <SidebarEntry Icon={LocalHospitalIcon} title={"COVID-19 Info Center"} />
            <SidebarEntry Icon={EmojiFlagsIcon} title={"Pages"} />
            <SidebarEntry Icon={PeopleIcon} title={"Friends"} />
            <a href={"https://messenger.com"} target={"_blank"}>
                <SidebarEntry Icon={ChatIcon} title={"Messages"} />
            </a>
            <SidebarEntry Icon={StoreIcon} title={"Marketplace"} />
            <SidebarEntry Icon={VideoLibraryIcon} title={"Videos"} />
            <SidebarEntry Icon={ExpandMoreIcon} />

        </div>
    );
}

export default Sidebar;