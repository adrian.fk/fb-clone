import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyD0Q9p6Soh0nDx2BsNPQVd7279sVG83Bb8",
    authDomain: "facebook-clone-e8482.firebaseapp.com",
    databaseURL: "https://facebook-clone-e8482.firebaseio.com",
    projectId: "facebook-clone-e8482",
    storageBucket: "facebook-clone-e8482.appspot.com",
    messagingSenderId: "371977974991",
    appId: "1:371977974991:web:a60feb763062549355b3ba",
    measurementId: "G-2GQ0S0N6NP"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();
const provider = new firebase.auth.GoogleAuthProvider();

export {auth, provider};
export default db;
